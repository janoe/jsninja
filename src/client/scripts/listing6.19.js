(function() {
    "use strict";
    /*jshint -W064 */ //W064: Missing 'new' prefix when invoking a constructor

    function User(first, last) {
        if (!(this instanceof User)) {
            return new User(first, last);
        }

        this.name = first + " " + last;
    }

    var name = "Rukia";

    var user = User("Paco", "Pil");

    assert(name === "Rukia", "Name was set to rukia");
    assert(user, "User instantiated");
    assert(user.name === "Paco Pil", "User Name correctly assigned");

})();
