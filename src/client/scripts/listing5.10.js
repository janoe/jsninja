(function() {
    "use strict";

    String.prototype.csv = String.prototype.split.partial(/,\s*/);

    var results = ("Mugan, Jin, Fuu").csv();

    assert(
        results[0] == "Mugan" &&
        results[1] == "Jin" &&
        results[2] == "Fuu",
        "The text were splitted properly"
    );

})();
