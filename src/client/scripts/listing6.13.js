(function() {
    "use strict";

    Number.prototype.add = function(num) {
        return this + num;
    };

    var n = 5;
    assert(n.add(3) === 8, "It works when the number is in a variable");
    assert((5).add(3) === 8, "Also works is the number is wrapped in parenthesis");
    //assert(5.add(3) === 8, "What about a simple literal ?"); //This does not work


})();
