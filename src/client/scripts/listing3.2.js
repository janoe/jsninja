/*jshint funcscope:true*/
/**
 * Created by janoe on 24/03/14.
 */
assert("true", "Antes de la declaración de outer");
assert(typeof outer === "function", "outer es una función y está en el contexto");
assert(typeof inner === "function", "inner es una función y está en el contexto");
assert(typeof a === "number", "Variable a está en el contexto");
assert(typeof b === "number", "Variable b está en el contexto");
assert(typeof c === "number", "Variable c está en el contexto");

function outer() {
    assert("true", "Dentro de Outer,antes de a");
    assert(typeof outer === "function", "outer es una función y está en el contexto");
    assert(typeof inner === "function", "inner es una función y está en el contexto");
    assert(typeof a === "number", "Variable a está en el contexto");
    assert(typeof b === "number", "Variable b está en el contexto");
    assert(typeof c === "number", "Variable c está en el contexto");

    var a = 1;

    assert("true", "Dentro de Outer,después de a");
    assert(typeof outer === "function", "outer es una función y está en el contexto");
    assert(typeof inner === "function", "inner es una función y está en el contexto");
    assert(typeof a === "number", "Variable a está en el contexto");
    assert(typeof b === "number", "Variable b está en el contexto");
    assert(typeof c === "number", "Variable c está en el contexto");


    function inner() { /*No hace nada*/ }

    var b = 2;

    assert("true", "Dentro de Outer,después de b");
    assert(typeof outer === "function", "outer es una función y está en el contexto");
    assert(typeof inner === "function", "inner es una función y está en el contexto");
    assert(typeof a === "number", "Variable a está en el contexto");
    assert(typeof b === "number", "Variable b está en el contexto");
    assert(typeof c === "number", "Variable c está en el contexto");

    if (a == 1) {
        var c = 3;

        assert("true", "Dentro de Outer,después de c, dentro del if");
        assert(typeof outer === "function", "outer es una función y está en el contexto");
        assert(typeof inner === "function", "inner es una función y está en el contexto");
        assert(typeof a === "number", "Variable a está en el contexto");
        assert(typeof b === "number", "Variable b está en el contexto");
        assert(typeof c === "number", "Variable c está en el contexto");
    }

    assert("true", "Dentro de Outer,después de c, ya fuera del if");
    assert(typeof outer === "function", "outer es una función y está en el contexto");
    assert(typeof inner === "function", "inner es una función y está en el contexto");
    assert(typeof a === "number", "Variable a está en el contexto");
    assert(typeof b === "number", "Variable b está en el contexto");
    assert(typeof c === "number", "Variable c está en el contexto");

}

assert("true", "Antes de ejecutar outer");
assert(typeof outer === "function", "outer es una función y está en el contexto");
assert(typeof inner === "function", "inner es una función y está en el contexto");
assert(typeof a === "number", "Variable a está en el contexto");
assert(typeof b === "number", "Variable b está en el contexto");
assert(typeof c === "number", "Variable c está en el contexto");

outer();

assert("true", "Después de ejecutar outer");
assert(typeof outer === "function", "outer es una función y está en el contexto");
assert(typeof inner === "function", "inner es una función y está en el contexto");
assert(typeof a === "number", "Variable a está en el contexto");
assert(typeof b === "number", "Variable b está en el contexto");
assert(typeof c === "number", "Variable c está en el contexto");
