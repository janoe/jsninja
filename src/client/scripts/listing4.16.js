function addMethod(object, name, fn) {
    var old = object[name];
    object[name] = function() {
        if (fn.length == arguments.length)
            return fn.apply(this, arguments);
        else if (typeof old == 'function')
            return old.apply(this, arguments);
    };
}

var ninjas = {
    values: ["Dean Edwards", "Sam Stephenson", "Alex Russell"]
};


addMethod(ninjas, 'find', function() {
    return this.values;
});

addMethod(ninjas, 'find', function(name) {
    var ret = [];
    for (var i = 0, arrayLength = this.values.length; i < arrayLength; i++) {
        if (this.values[i].indexOf(name) > -1) {
            ret.push(this.values[i]);
        }
    }

    return ret;
});

addMethod(ninjas, 'find', function(firstName, lastName) {
    var ret = [];
    for (var i = 0, arrayLength = this.values.length; i < arrayLength; i++) {
        if (this.values[i] === firstName + " " + lastName) {
            ret.push(this.values[i]);
        }
    }
    return ret;
});


assert(ninjas.find().length === 3, "Find sin parametros devuelve todos los valores");
assert(ninjas.find("Dean").length === 1, "Find con un parametro busca por nombre");
assert(ninjas.find("Sam").length === 1, "Find con un parametro busca por nombre");
assert(ninjas.find("Alex").length === 1, "Find con un parametro busca por nombre");
assert(ninjas.find("Edwards").length === 1, "Find con un parametro busca por nombre");
assert(ninjas.find("Stephenson").length === 1, "Find con un parametro busca por nombre");
assert(ninjas.find("Alex Russell").length === 1, "Find con un parametro busca por nombre");
assert(ninjas.find("Sam", "Stephenson").length === 1, "Find con dos parametros busca por nombre y apellido");
assert(ninjas.find("Dean", "Edwards").length === 1, "Find con dos parametros busca por nombre y apellido");
assert(ninjas.find("Dean", "Stephenson").length === 0, "Find con dos parametros busca por nombre y apellido");
assert(ninjas.find().length === 3, "Find sin parametros devuelve todos los valores");
console.log(ninjas.find("Alex", "Russell", "Jr"));
assert(ninjas.find("Alex", "Russell", "Jr") === undefined, "Found nothing");
