var elements = {
    length: 0,
    add: function(element) {
        Array.prototype.push.call(this, element);
    },
    gather: function(id) {
        this.add(document.getElementById(id));
    }
};

elements.gather("first");
assert(elements.length == 1 && elements[0].nodeType,
    "Verify that we have an element in our stash");

elements.gather("second");
assert(elements.length == 2 && elements[1].nodeType,
    "Verify the other insertion");
