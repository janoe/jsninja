(function() {
    "use strict";

    Object.prototype.keys = function() {
        var keys = [];
        for (var i in this) {
            if (this.hasOwnProperty(i)) {
                keys.push(i);
            }
        }
        return keys;
    };

    var obj = {
        a: 1,
        b: 2,
        c: 3
    };

    log(obj.keys());
    assert(obj.keys().length === 3, "There are three properties in this object");


})();
