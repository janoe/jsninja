function Ninja() {
    var feints = 0;

    this.getFeints = function() {
        return feints;
    };

    this.feint = function() {
        feints++;
    };
}

var ninja = new Ninja();

ninja.feint();

assert(ninja.getFeints() == 1, "We're able to access the internal feint count.");
console.log(ninja.feints);
assert(ninja.feints === undefined, "And the private data is inaccessible for us.");
