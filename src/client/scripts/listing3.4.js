function Ninja() {
    this.skulk = function() {
        return this;
    };
}

var ninja1 = new Ninja();
var ninja2 = new Ninja();

assert(ninja1.skulk() === ninja1, "El scope como constructor es el propio objeto");
assert(ninja2.skulk() === ninja2, "El scope como constructor es el propio objeto");

var ninja3 = Ninja();

assert(window.skulk() === window, "El scope al llamar a un constructor como función normal es el global");
assert(ninja3 === undefined, "Ninja no está definido puesto que la función no devuelve nada");

function Ninja1() {

    if (this === window) {
        log('Coooo, llámame como me tienes que llamar');
        return new Ninja1();
    }

    this.skulk = function() {
        return this;
    };
}

var ninja4 = Ninja1();
assert(ninja4.skulk() === ninja4, "El scope como constructor es el propio objeto");
