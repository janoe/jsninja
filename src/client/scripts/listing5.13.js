(function() {
    "use strict";

    Function.prototype.start = function() {
        this._time = new Date();
    };

    Function.prototype.getTime = function() {
        var now = new Date();
        return now - this._time;
    };

    Function.prototype.memoized = function(key) {
        isPrime.start();
        this._values = this._values || {};
        return this._values[key] !== undefined ?
            this._values[key] :
            this._values[key] = this.apply(this, arguments);
    };

    function isPrime(num) {
        isPrime.start();
        var prime = num != 1;
        for (var i = 2; i < num; i++) {
            if (num % i === 0) {
                console.log(i);
                prime = false;
                break;
            }
        }
        return prime;
    }

    var start = new Date();
    var delayed = new Date();


    assert(isPrime(78837901), "The function works; 78837901 is prime: " + isPrime.getTime() + "ms");
    assert(!isPrime(78837902), "The function works; 78837902 is not prime: " + isPrime.getTime() + "ms");
    assert(isPrime.memoized(78837901), "The answer has been cached: " + isPrime.getTime() + "ms");
    assert(isPrime.memoized(78837901), "The getting result from cache: " + isPrime.getTime() + "ms");



})();
