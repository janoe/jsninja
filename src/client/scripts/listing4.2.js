function chirp(n) {
    return n > 1 ? chirp(n - 1) + "-chirp" : "chirp";
}

var EXPECTED_TEXT = "chirp-chirp-chirp";
assertEquals(chirp(3), EXPECTED_TEXT, EXPECTED_TEXT);
