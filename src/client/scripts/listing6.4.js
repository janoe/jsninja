(function() {
    "use strict";

    function Ninja() {
        this.swung = true;

        this.swingSword = function() {
            return !this.swung;
        };
    }

    var ninja = new Ninja();

    Ninja.prototype.swingSword = function() {
        return this.swung;
    };

    assert(!ninja.swingSword(), "Called the instance method, not the prototype method.");


})();
