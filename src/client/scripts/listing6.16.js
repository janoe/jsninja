(function() {
    "use strict";

    function User(first, last) {
        this.name = first + " " + last;
    }

    var user = new User("Paco", "Pil");

    assert(user, "User instantiated");
    assert(user.name == "Paco Pil", "User name correctly assigned");


})();
