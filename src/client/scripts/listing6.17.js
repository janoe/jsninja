(function() {
    /*jshint -W064 */ //W064: Missing 'new' prefix when invoking a constructor

    function User(first, last) {
        this.name = first + " " + last;
    }

    var name = "Manolo";

    var user = User("Paco", "Pil");

    assert(!user, "La instancia de usuario no ha sido creada");
    assert(name === "Manolo", "En este contexto sigue estando manolo");
    assert(window.name === "Paco Pil", "La llamada a la función a metido en el contexto global Paco Pil");

})();
