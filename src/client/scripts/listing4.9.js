/*jshint -W093 */
function isPrime(value) {
    if (!isPrime.answers) isPrime.answers = {};
    if (isPrime.answers[value] !== null) {
        return isPrime.answers[value];
    }

    var prime = value != 1; //1 nunca puede ser primo.
    for (var i = 2; i < value; i++) {
        if (value % i === 0) {
            prime = false;
            break;
        }
    }

    return isPrime.answers[value] = prime;
}

assert(isPrime(5), "5 es primo.");
assert(isPrime.answers[5], "5 es primo y está cacheado.");
