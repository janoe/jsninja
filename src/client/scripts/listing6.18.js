(function() {
    function Test() {
        return this instanceof Test;
    }

    /*jshint -W064 */ //Missing 'new' prefix when invoking a constructor
    assert(!Test(), "We didn´t instantiate, so return false");
    assert(new Test(), "We do instantiate, so return true");


})();
