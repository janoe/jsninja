var ninja = {
    chirp: function(n) {
        return n > 1 ? this.chirp(n - 1) + "-chirp" : "chirp";
    }
};

var samurai = {
    chirp: ninja.chirp
};

ninja = {};

try {
    assert(samurai.chirp(3) == "chirp-chirp-chirp",
        "Is this going to work?");
} catch (e) {
    assert(false,
        "Uh, this is not good,what the hell did ninja go? ");
}
