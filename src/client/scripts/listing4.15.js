function addMethod(object, name, fn) {
    var old = object[name];
    object[name] = function() {
        if (fn.length == arguments.length)
            return fn.apply(this, arguments);
        else
            return old.apply(this, arguments);
    };
}

var ninja = {};
addMethod(ninja, 'whatever', function() {});
addMethod(ninja, 'whatever', function(a) {});
addMethod(ninja, 'whatever', function(a, b) {});
