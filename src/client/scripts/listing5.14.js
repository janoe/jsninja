(function() {
    "use strict";

    Function.prototype.memoized = function(key) {
        log('Dentro de memoized');
        this._values = this._values || {};

        if (this._values[key] !== undefined) {
            console.log(key + ' Cacheada');
        } else {
            console.log(key + ' No cacheada');
        }

        return this._values[key] !== undefined ?
            this._values[key] :
            this._values[key] = this.apply(this, arguments);
    };

    Function.prototype.memoize = function(key) {
        log('Dentro de memoize');
        var fn = this;
        return function() {
            return fn.memoized.apply(fn, arguments);
        };
    };

    var isPrime = (function(num) {
        log('Dentro de is Prime');
        var prime = num != 1;
        for (var i = 2; i < num; i++) {
            if (num % i === 0) {
                prime = false;
                break;
            }
        }
        return prime;
    }).memoize();

    assert(isPrime(17), "17 is prime no cacheado");
    assert(isPrime(17), "17 is prime cacheado");

})();
