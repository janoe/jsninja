function creep() {
    return this;
}

assert(creep() === window, "El scope de creep es el global");

var sneak = creep;

assert(sneak() === window, "El scope de sneak también es el global");

var ninja1 = {
    skulk: creep
};

assert(ninja1.skulk() === ninja1, "El scope de ninja1.skulk() es ninja1");

var ninja2 = {
    skulk: creep
};

assert(ninja2.skulk() === ninja2, "El scope de ninja1.skulk() es ninja2");
