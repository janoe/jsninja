(function() {
    "use strict";

    function Person() {}
    Person.prototype.dance = function() {};

    function Ninja() {}
    Ninja.prototype = new Person();

    var ninja = new Ninja();
    assert(ninja instanceof Ninja,
        "Ninja receives functionality from the Ninja prototype");
    assert(ninja instanceof Person,
        "Ninja receives functionality from the Person prototype");
    assert(ninja instanceof Object,
        "Ninja receives functionality from the Object prototype");


})();
