(function() {
    "use strict";

    HTMLElement.prototype.remove = function() {
        if (this.parentNode)
            this.parentNode.removeChild(this);
    };

    var a = document.getElementById("a");
    a.parentNode.removeChild(a);

    document.getElementById("b").remove();

    assert(!document.getElementById("a"), "a is gone");
    assert(!document.getElementById("b"), "b is gone");



})();
