var store = {
    nextId: 1,

    cache: [],

    add: function(fn) {
        if (!fn.id) {
            fn.id = store.nextId++;
            return !!(store.cache[fn.id - 1] = fn);
        }
    },
    count: function() {
        return store.cache.length;
    }
};

function ninja() {}

assert(store.add(ninja),
    "Function was safely added.");

assertEquals(1, store.count(), "Size is 1.");

assert(!store.add(ninja),
    "But it was only added once.");

assertEquals(1, store.count(), "Size still being 1.");
