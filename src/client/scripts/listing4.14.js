function multiMax(multi) {
    return multi * Math.max.apply(Math, Array.prototype.slice.call(arguments, 1));
}

assert(multiMax(3, 1, 2, 3) === 9, "3 * 3 = 9 (First arg, by largest.)");
assert(multiMax(3, 3, 2, 1) === 9, "3 * 3 = 9 (First arg, by largest.)");
assert(multiMax(3, 1, 3, 2) === 9, "3 * 3 = 9 (First arg, by largest.)");
