var ninja = {
    chirp: function signal(n) {
        return n > 1 ? signal(n - 1) + "-chirp" : "chirp";
    }
};

var expectedText = "chirp-chirp-chirp";
assertEquals(ninja.chirp(3), expectedText,
    "Works as expect it to!");

var samurai = {
    chirp: ninja.chirp
};

ninja = {};

assertEquals(samurai.chirp(3), expectedText,
    "Works as expect it to!");
