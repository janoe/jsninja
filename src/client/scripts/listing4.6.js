var ninja = function myNinja() {
    assert(ninja == myNinja, "This function is named two things at once!!");
};

ninja();

assert(typeof myNinja == "undefined", "But myNinja isn't defined outside of the function.");
assert(typeof window.ninja == "function", "Ninja is defined as function on global scope.");
