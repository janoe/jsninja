(function() {
    /*jshint -W064 */ //W064: Missing 'new' prefix when invoking a constructor.
    "use strict";

    function Ninja() {}

    Ninja.prototype.swingSword = function() {
        return true;
    };

    var ninja1 = Ninja();
    assert(ninja1 === undefined, "No instance of ninja defined");

    var ninja2 = new Ninja();
    assert(ninja2 && ninja2.swingSword, "Instance exists and method is callable");

})();
