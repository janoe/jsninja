function chirpf(n) {
    return (n > 1) ? this.chirp(n - 1) + "-chirp" : "chirp";
}

var ninja = {
    chirp: chirpf
};

var expectedText = "chirp-chirp-chirp";
assert(ninja.chirp(3) === expectedText, expectedText);
