function assert(value, desc) {
    var results = document.getElementById("results");
    if (!results) {
        results = document.createElement("ul");
        document.body.appendChild(results);
        results.id = "results";
    }



    var li = document.createElement("li");
    li.className = value ? "pass" : "fail";
    li.appendChild(document.createTextNode(desc));
    results.appendChild(li);
    if (!value) {
        log(desc + ': ERROR');
        li.parentNode.parentNode.className = "fail";
    } else {
        log(desc + ': OK');
    }
    return li;
}

function assertEquals(value1, value2, desc) {
    var value = value1 === value2;
    if (!value) {
        log('ERROR: expected: ');
        log(value1);
        log('ERROR: received: ');
        log(value2);
    }

    assert(value, desc);

}
