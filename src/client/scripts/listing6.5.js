(function() {
    "use strict";

    function Ninja() {}

    var ninja = new Ninja();

    assert(typeof ninja === "object",
        "Type of the instance is object");

    assert(ninja instanceof Ninja,
        "Instanceof identifies constructor");

    assert(ninja.constructor == Ninja,
        "The ninja object was created by the Ninja function.");


})();
