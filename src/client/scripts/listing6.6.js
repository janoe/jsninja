(function() {
    "use strict";

    function Ninja() {}

    var ninja = new Ninja();
    var ninja2 = new ninja.constructor();

    assert(ninja2 instanceof Ninja, "Its a ninja");
    assert(ninja !== ninja2, "But not the same ninja");


})();
