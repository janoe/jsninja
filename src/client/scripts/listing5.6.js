(function() {
    "use strict";

    function animateIt(elementId) {
        var tick = 0;
        var elem = document.getElementById(elementId);
        var initialLeft = elem.style.left;
        var initialTop = elem.style.top;

        var timer = setInterval(function() {
            if (tick < 100) {
                elem.style.left = initialLeft + tick + "px";
                elem.style.top = initialTop + tick + "px";
                tick++;
            } else {
                clearInterval(timer);
                assert(tick == 100, "Tick accesed via closure");
                assert(elem, "elem accesed via closure");
                assert(timer, "timer accesed via closure");
            }
        }, 10);

        window.myElement = elem;
    }

    animateIt("box");
    //animateIt("box1");
})();
