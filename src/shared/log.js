function log() {
    try {
        console.log.apply(console, arguments); //Tries to log message using the most common method.
    } catch (ex) { //Catches any failure of logging
        try {
            opera.postError.apply(opera, arguments); //Tries to log the opera way
        } catch (ex1) {
            alert(Array.prototype.join.call(arguments, "")); //Uses alert if all else fails
        }
    }
}
